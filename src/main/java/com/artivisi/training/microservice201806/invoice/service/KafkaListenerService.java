package com.artivisi.training.microservice201806.invoice.service;

import com.artivisi.training.microservice201806.invoice.dto.InvoiceRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaListenerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListenerService.class);

    @Autowired private ObjectMapper objectMapper;
    @Autowired private InvoiceService invoiceService;

    @KafkaListener(topics = "${kafka.topic.invoice-request}")
    public void handleInvoiceRequest(String message){
        try {
            LOGGER.info("Terima message : {}", message);
            InvoiceRequest invoiceRequest = objectMapper.readValue(message, InvoiceRequest.class);
            LOGGER.info("Object invoice request : {}", invoiceRequest);
            invoiceService.createInvoice(invoiceRequest);
        } catch (Exception err) {
            LOGGER.error(err.getMessage(), err);
        }
    }
}
