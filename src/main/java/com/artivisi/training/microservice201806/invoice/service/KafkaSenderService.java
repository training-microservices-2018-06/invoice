package com.artivisi.training.microservice201806.invoice.service;

import com.artivisi.training.microservice201806.invoice.dto.InvoiceResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSenderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSenderService.class);

    @Value("${kafka.topic.invoice-response}")
    private String topicInvoiceResponse;

    @Autowired private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired private ObjectMapper objectMapper;

    public void kirimInvoiceResponse(InvoiceResponse invoiceResponse) {
        try {
            LOGGER.info("Invoice Response : {}", invoiceResponse);
            String msg = objectMapper.writeValueAsString(invoiceResponse);
            LOGGER.info("JSON Invoice Response : {}", msg);
            kafkaTemplate.send(topicInvoiceResponse, msg);
        } catch (Exception err) {
            LOGGER.error(err.getMessage(), err);
        }
    }
}
